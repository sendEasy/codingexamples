using System;
using System.Collections.Generic;

namespace CodingExamples
{
    public class GradeBook
    {
        //Constructor has no return type and hasthe same name of the Class  - in here I can initiliaze the Fields for that class
        public GradeBook(string name)
        {
            grades = new List<double>();
            //Dotnet can distinguish between a field and a parameter in this instance
            this.Name = name;
        }
        public Statistics GradeBookStatistics()
        {

            var statistics = new Statistics();

            //Calculcate Sum Of Grades
            statistics.SumOfGrade = ComputeGradeSum(grades);
            //Calculcate Average Of Grades
            statistics.Average = ComputeGradeAverage(statistics.SumOfGrade , grades.Count);
            //Return Min and Max Values
            statistics.MinMaxGrades = DetectMinMaxGrades(grades);

            return statistics;
            
        }
        public void AddGradeToBook(double grade)
        {
            //if(grade >= 0 && grade <=100){

            grades.Add(grade);

            //}
        }
        public double ComputeGradeAverage(double sum, int countOfValues)
        {

            var average = 0.0;

            if (sum > 0 && countOfValues > 0)
            {

                average = sum / countOfValues;
            }

            return average;

        }
        public double ComputeGradeSum(List<double> grades)
        {

            var sum = 0.00;


            foreach (var grade in grades)
            {

                if (!Double.IsNaN(grade / 0))
                    sum += grade;

            }

            return sum;
        }
        public Tuple<double, double> DetectMinMaxGrades(List<double> grades)
        {

            var minMark = double.MaxValue;
            var maxMark = double.MinValue;

            foreach (var grade in grades)
            {                
                minMark = Math.Min(grade, minMark);
                maxMark = Math.Max(grade, maxMark);
            }

            return new Tuple<double, double>(minMark, maxMark);
        }

        #region Fields
        private List<double> grades;
        public string Name;

        #endregion
    }
}