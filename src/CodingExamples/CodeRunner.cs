﻿using System;

namespace CodingExamples
{
    class CodeRunner
    {
        public static void Main(string[] args)
        {
            var gradeBook = new GradeBook("Franco's Grade Book");

            var gradebook1 = new GradeBook("Book 1");
            var gradebook2 = gradebook1;


            Console.WriteLine($"gradebook1 Value is {gradebook1.Name}");
            Console.WriteLine($"gradebook2 Value is {gradebook2.Name}");


            
            gradeBook.AddGradeToBook(80);
            gradeBook.AddGradeToBook(23.5);
            gradeBook.AddGradeToBook(45);
            gradeBook.AddGradeToBook(20);
            gradeBook.AddGradeToBook(45.8);
            gradeBook.AddGradeToBook(77.9);
            

            var statistics = gradeBook.GradeBookStatistics();

            Console.WriteLine($"Max Value is {statistics.MinMaxGrades.Item2}");
            Console.WriteLine($"Min Value is {statistics.MinMaxGrades.Item1}");
            Console.WriteLine($"Sum Value is {statistics.SumOfGrade}");
            Console.WriteLine($"Averag Value is {statistics.Average}");
        }
    }
}
