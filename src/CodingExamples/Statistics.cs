using System;

namespace CodingExamples
{
    public class Statistics
    {
        public double SumOfGrade;
        public Tuple<double, double> MinMaxGrades;
        public double Average;
    }
}