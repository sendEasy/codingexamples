using System;
using System.Collections.Generic;
using System.Linq;

public class CodingTask
{

    public int SoldierThatCanReport(int[] ranks)
    {

        int soldierCanReport = 0;
        var groupByRank = ranks.GroupBy(r => r);
        
        foreach (var rank in groupByRank)
        {
            for (int i = 0; i < ranks.Length; i++)
            {
                if (ranks[i] > 0)
                {
                    if (ranks[i] == rank.Key + 1)
                    {
                        soldierCanReport += rank.Count();
                        break;
                    }
                }

            }
        }
        return soldierCanReport;
    }

    public int CalculateMissingMinValueFromArray(int[] A)
    {

        List<int> tempListOfPositive = new List<int>();
        bool allNegative = true;
        int minimumMissingValue = 1;

        for (int i = 0; i < A.Length; i++)
        {
            if (A[i] > 0)
            {
                tempListOfPositive.Add(A[i]);
                allNegative = false;

            }
        }


        if (allNegative == false)
        {
            tempListOfPositive.Sort();
            int mapInt;

            for (int i = 0; i < tempListOfPositive.Count; i++)
            {
                mapInt = i + 1;

                if (mapInt != tempListOfPositive[i])
                {

                    minimumMissingValue = mapInt;
                    break;

                }
            }

        }
        return minimumMissingValue;

    }
}