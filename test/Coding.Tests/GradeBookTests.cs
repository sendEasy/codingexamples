using System;
using Xunit;

namespace CodingExamples.Tests
{
    public class GradeBookTests
    {

        //Attribute - think as a little bit of data attached to this method
        [Fact]
        public void TestGradeBook()
        {
            //Arrange

            // REFERENCE TYPE - everytime you work with a class. 
            // Memory available will create a space for the variable 
            // gradebook and it will hold a valueallocated in the memory. 
            // Gradebook is the reference of the the book object where it lives on the computer/vm memory
            // the fields are references as well
            var gradebook = new GradeBook("TestRun");


            // VALUE TYPE  - Storing the Value in the memory
            var expectedAverage = 40.0;
            var expectedSum = 120.0;
            var expectedMinGrade = 10.0;
            var expectedMaxGrade = 70.0;
                   
            gradebook.AddGradeToBook(10);
            gradebook.AddGradeToBook(70);
            gradebook.AddGradeToBook(40);

            //Act
            var statistics = gradebook.GradeBookStatistics();
            

            //Assert
            Assert.Equal(expectedMinGrade, statistics.MinMaxGrades.Item1);
            Assert.Equal(expectedMaxGrade, statistics.MinMaxGrades.Item2);
            Assert.Equal(expectedAverage, statistics.Average);
            Assert.Equal(expectedSum, statistics.SumOfGrade);





        }
    }
}
