using System;
using Xunit;

namespace CodingExamples.Tests
{

    public class TypeTest
    {
        
        [Fact]
        public void StringActLikeValueTypes()
        {
            string name = "Franco";

            var upperName = MakeUpperCase(name);

            Assert.Equal("FRANCO", upperName);
            Assert.Equal("Franco", name);
        }

        [Fact]
        public void ValueTypesAlsoPassByValue()
        {
            var x = GetInt();
            SetInt(ref x);

            Assert.Equal(42, x);
        }

        [Fact]
        public void GetBookReturnDifferentObjects()
        {
            //Arrange

            // REFERENCE TYPE - everytime you work with a class. 
            // Memory available will create a space for the variable 
            // gradebook and it will hold a valueallocated in the memory. 
            // Gradebook is the reference of the the book object where it lives on the computer/vm memory
            // the fields are references as well;

            var gradebook1 = GetBook("Book 1");
            var gradebook2 = GetBook("Book 2");

            Assert.Equal("Book 1", gradebook1.Name);
            Assert.Equal("Book 2",gradebook2.Name);
            Assert.NotSame(gradebook1,gradebook2);

        }

        [Fact]
        public void TwoVariablesCanReferenceSameObject()
        {

            //Its not a copy - its a value copy  - same pointer for that value;
            var gradebook1 = GetBook("Book 1");
            var gradebook2 = gradebook1;

            Assert.Same(gradebook1,gradebook2);
            Assert.True(Object.ReferenceEquals(gradebook1,gradebook2));
        }

        [Fact]
        public void CanSetNameFromReference()
        {

            //Its not a copy - its a value copy  - same pointer for that value;
            var gradebook1 = GetBook("Book 1");
            SetName(gradebook1, "New Name");
            
            Assert.Equal("New Name",gradebook1.Name);
            
        }

        [Fact]
        public void CsharpIsPassedByValue()
        {

            //Its not a copy - its a value copy  - same pointer for that value;
            var gradebook1 = GetBook("Book 1");
            GetBookSetName(gradebook1, "New Name");
            
            Assert.NotEqual("New Name",gradebook1.Name);
            
        }
        [Fact]
        public void CsharpCanPassByReference()
        {

            //Its not a copy - its a value copy  - same pointer for that value;
            var gradebook1 = GetBook("Book 1");
            GetBookSetNameByRef(ref gradebook1, "New Name");
            
            Assert.Equal("New Name",gradebook1.Name);
            
        }

        [Fact]
        public void CsharpCanPassByOut()
        {

            //Its not a copy - its a value copy  - same pointer for that value;
            var gradebook1 = GetBook("Book 1");
            GetBookSetNameByOut(out gradebook1, "New Name");
            
            Assert.Equal("New Name",gradebook1.Name);
            
        }

        private void SetName(GradeBook gradebookInstance, string name)
        {
            gradebookInstance.Name = name;
        }

        private void GetBookSetName(GradeBook gradebookInstance, string name)
        {
            gradebookInstance = new GradeBook(name);
            gradebookInstance.Name = name;
        }

        private void GetBookSetNameByRef(ref GradeBook gradebookInstance, string name)
        {
            //No Need of instanciating a new GradeBook objet
            gradebookInstance.Name = name;
        }

        private void GetBookSetNameByOut(out GradeBook gradebookInstance, string name)
        {
            // When using out you wil need to instanciate it;
            gradebookInstance = new GradeBook(name);
            gradebookInstance.Name = name;
        }
        
        private int GetInt()
        {
            return 3;
        }

        private void SetInt(ref int y)
        {
            y =42;
        }

        private string  MakeUpperCase( string parameter)
        {
            return parameter.ToUpper();
        }
        GradeBook GetBook(string name)
        {
            return new GradeBook(name);
        }

    }
}
